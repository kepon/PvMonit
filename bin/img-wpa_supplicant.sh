#!/bin/bash

if [ -f /boot/hostapd.conf ]; then
	fs_mode=$(mount | sed -n -e "s/^.* on \/ .*(\(r[w|o]\).*/\1/p")
	if [ "$fs_mode" != "rw" ]; then
		mount -o remount,rw /
	fi
	cat /boot/hostapd.conf > /etc/hostapd/hostapd.conf
	cp /etc/dhcpcd.conf_hotspot /etc/dhcpcd.conf
	cp /etc/default/hostapd_hotspot /etc/default/hostapd
	rm /boot/hostapd.conf
	echo "ici"
	if ! (($?)); then
		#~ /sbin/reboot
		exit 0
	fi
fi


# Si l'utilisateur à déposé un fichier wpa_supplicant.conf dans boot
if [ -f /boot/wpa_supplicant.conf ]; then
	fs_mode=$(mount | sed -n -e "s/^.* on \/ .*(\(r[w|o]\).*/\1/p")
	if [ "$fs_mode" == "ro" ]; then
		/bin/mount -o remount,rw /
	fi
	cp /etc/dhcpcd.conf_box /etc/dhcpcd.conf
	cp /etc/default/hostapd_box /etc/default/hostapd
	cat /boot/wpa_supplicant.conf > /etc/wpa_supplicant/wpa_supplicant.conf
	rm /boot/wpa_supplicant.conf 
	if ! (($?)); then
		/sbin/reboot
	fi
fi
