#!/bin/bash

# inspirer : https://www.framboise314.fr/mesure-de-temperature-1-wire-ds18b20-avec-le-raspberry-pi/

# On liste les sondes de la famille 28
FILES=`ls /sys/bus/w1/devices/w1_bus_master1/ | grep '^28'`

for file in $FILES
do
	ID=`echo $file`
	# Récupération des information
	GETDATA=`cat /sys/bus/w1/devices/w1_bus_master1/$file/w1_slave`
	GETDATA1=`echo "$GETDATA" | grep crc`
	GETDATA2=`echo "$GETDATA" | grep t=`
	# Récupération de la température
	TEMP=`echo $GETDATA2 | grep "t=" | awk -F "t=" '{printf("%.1f\n", $2/1000)}'`
	#
	# test SI crc est à  'YES' 
	if [ `echo $GETDATA1 | sed 's/^.*\(...\)$/\1/'` == "YES" ]
	then
		echo "ID=$ID ; TEMP=$TEMP"
	else
		echo "ID=$ID ; CRC NOK"
	fi
done
exit 0
