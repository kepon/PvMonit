#!/bin/bash

# inspirer : https://www.framboise314.fr/mesure-de-temperature-1-wire-ds18b20-avec-le-raspberry-pi/

# Récupération des information
GETDATA=`cat /sys/bus/w1/devices/w1_bus_master1/$1/w1_slave`
GETDATA1=`echo "$GETDATA" | grep crc`
GETDATA2=`echo "$GETDATA" | grep t=`
# Récupération de la température
TEMP=`echo $GETDATA2 | grep "t=" | awk -F "t=" '{printf("%.1f\n", $2/1000)}'`
#
# test SI crc est à  'YES' 
if [ `echo $GETDATA1 | sed 's/^.*\(...\)$/\1/'` == "YES" ]
then
	echo $TEMP
	exit 0
else
	echo "NOK"
	exit 255
fi
