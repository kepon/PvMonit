<?php

$id="28-011921191050";     // GPIO pin number
$cmd='/bin/bash '.  $config['dir']['bin'].'ds18b20.sh '.$id;

exec($cmd, $sortie, $retour);
if ($retour != 0){
    trucAdir(1, 'Erreur '.$retour.' à l\'exécussion du programme .'.$cmd);
} else {
    $nb=0;
    trucAdir(4, 'Le script retourne '.$sortie[0]);
    $array_data[$nb]['id']='THomeT'; 
    $array_data[$nb]['screen']=1;
    $array_data[$nb]['smallScreen']=0;
    $array_data[$nb]['value']=$sortie[0];
    $array_data[$nb]['units']='°C';
    $array_data[$nb]['desc']='Température THome';
}

return $array_data;

?>
